package com.tuwer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 土味儿
 * Date 2022/5/18
 * @version 1.0
 */
@SpringBootApplication
public class Resource_b_8002 {
    public static void main(String[] args) {
        SpringApplication.run(Resource_b_8002.class, args);
    }
}
